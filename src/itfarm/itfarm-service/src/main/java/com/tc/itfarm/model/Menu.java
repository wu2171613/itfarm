package com.tc.itfarm.model;

import java.io.Serializable;
import java.util.Date;

public class Menu implements Serializable {
    private Integer recordId;

    private String name;

    private String url;

    private String remark;

    private Date modifyTime;

    private Date createTime;

    private static final long serialVersionUID = 1L;

    public Menu(Integer recordId, String name, String url, String remark, Date modifyTime, Date createTime) {
        this.recordId = recordId;
        this.name = name;
        this.url = url;
        this.remark = remark;
        this.modifyTime = modifyTime;
        this.createTime = createTime;
    }

    public Menu() {
        super();
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}