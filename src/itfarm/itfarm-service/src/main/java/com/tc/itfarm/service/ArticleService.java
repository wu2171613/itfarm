package com.tc.itfarm.service;

import java.util.List;

import com.tc.itfarm.api.enums.QueryArticleEnum;
import com.tc.itfarm.api.model.Page;
import com.tc.itfarm.api.model.PageList;
import com.tc.itfarm.model.Article;
import com.tc.itfarm.model.ext.ArticleEx;

public interface ArticleService extends BaseService<Article> {
	PageList<Article> selectArticleByPage(Page page, Integer categoryId, String title);
	
	Article select(Integer recordId);
	
	void save(Article article);
	
	List<Article> selectArticles(QueryArticleEnum e, int count);
	
	ArticleEx selectLast(String title);
	
	ArticleEx selectNext(String title);
}
