package com.tc.itfarm.service;

import com.tc.itfarm.model.Log;

import java.util.List;

/**
 * Created by wangdongdong on 2016/8/17.
 */
public interface LogService extends BaseService<Log> {

    List<Log> selectByType(String type);

}
