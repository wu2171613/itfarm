package com.tc.itfarm.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.tc.itfarm.api.model.Page;
import com.tc.itfarm.api.model.PageList;
import com.tc.itfarm.api.util.DateUtils;
import com.tc.itfarm.api.util.PageQueryHelper;
import com.tc.itfarm.dao.CategoryDao;
import com.tc.itfarm.model.Category;
import com.tc.itfarm.model.CategoryCriteria;
import com.tc.itfarm.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Resource
	private CategoryDao categoryDao;
	
	@Override
	public Category select(Integer id) {
		return categoryDao.selectById(id);
	}

	@Override
	public List<Category> selectAll() {
		CategoryCriteria criteria = new CategoryCriteria();
		return categoryDao.selectByCriteria(criteria);
	}

	@Override
	public PageList<Category> selectByPage(Page page) {
		CategoryCriteria criteria = new CategoryCriteria();
		return PageQueryHelper.queryPage(page, criteria, categoryDao, null);
	}

	@Override
	public void save(Category category) {
		if (category.getRecordId() == null) {
			category.setCreateTime(DateUtils.now());
			categoryDao.insert(category);
		} else {
			categoryDao.updateById(category);
		}
	}

	@Override
	public void delete(Integer recordId) {
		categoryDao.deleteById(recordId);
	}

}
