package com.tc.itfarm.dao;

import com.tc.itfarm.api.model.SingleTableDao;
import com.tc.itfarm.model.Category;
import com.tc.itfarm.model.CategoryCriteria;

public interface CategoryDao extends SingleTableDao<Category, CategoryCriteria> {
}